import AsyncStorage from '@react-native-async-storage/async-storage';
const API = '172.18.9.229:5001/gastos'
import {Alert} from 'react-native'

/*async function saveItem(listItem, _id) {
    listItem._id = _id ? _id : new Date().getTime()
    const savedItems = await getItems();

    if (_id) {
        const index = await savedItems.findIndex(item => item._id === _id);
        savedItems[index] = listItem;
    }
    else
        savedItems.push(listItem);
    console.log(listItem)
    return AsyncStorage.setItem('items', JSON.stringify(savedItems));
}*/

async function saveItem(listItem) {
    listItem._id = new Date().getTime();
    const savedItems = await getItems();
    savedItems.push(listItem);
    console.log(listItem)
    return AsyncStorage.setItem('items', JSON.stringify(savedItems))
    .then(Alert.alert(`Item: ${listItem.title} foi salvo com sucesso! ${listItem._id}`));
}

async function editItem(listItem, _id) {
    listItem._id = _id ? _id : new Date().getTime()
    const savedItems = await getItems();

    if (_id) {
        const index = await savedItems.findIndex(item => item._id === _id);
        savedItems[index] = listItem;
    }
    else
        savedItems.push(listItem);
    console.log(listItem)
    return AsyncStorage.setItem('items', JSON.stringify(savedItems));
}

function getItems() {
    return AsyncStorage.getItem('items')
        .then(response => {
            if (response)
                return Promise.resolve(JSON.parse(response));
            else
                return Promise.resolve([]);
        })
}

async function getItem(_id) {
    const savedItems = await getItems();
    return savedItems.find(item => item._id === _id);
}

async function deleteItem(_id) {
    let savedItems = await getItems();
    const index = await savedItems.findIndex(item => item._id === _id);
    savedItems.splice(index, 1);
    return AsyncStorage.setItem('items', JSON.stringify(savedItems));
}

module.exports = {
    saveItem,
    editItem,
    getItems,
    getItem,
    deleteItem
}